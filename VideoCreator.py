import sqlite3
import time
import copy
from xml.etree import ElementTree
import os


from YouTube import YouTubeAPICalls
from TTS import googleTTS
from DBstuff import DBres
from Reddit import Redditres
from Video import Videores

class subreddit_details:
	def __init__(self, title, description, keywords, backgroundfile, logofile, color, musicfile):
		self.title = title
		self.description = description
		self.keywords = keywords
		self.category = 22
		self.privacyStatus = "public"
		self.file = "output.mp4"
		self.backgroundfile = backgroundfile
		self.logofile = logofile
		self.color = color
		self.musicfile = musicfile


def format_time(start,end):
	difference = end - start
	minutes = difference/60
	minutes = int(minutes)
	remainder_seconds = difference % 60
	remainder_seconds = int(remainder_seconds)
	print("Time elapsed: " + str(minutes)+":"+str(remainder_seconds))

def pause_human_editing(globalchildrenlist):
	#should be able to pause and recursively pick and index
	#and see title and self text
	going = True
	#print(globalchildrenlist)
	while going:
		answer = input("input a number to see the self text and title, or C to continue: ")

		if answer=="c" or answer == "C":
			going = False
		else:
			title = globalchildrenlist[int(answer)].get("data").get("title")
			selftext = globalchildrenlist[int(answer)].get("data").get("selftext")
			print("Title: " + title)
			print("Selftext: " + selftext)
			print("")


def clean_mp3_files():
	#getting an error about other processes using these files???
	for x in range(0, len(videotextarray)):
		os.remove("MP3s/"+str(x)+".mp3")
		print("\r Deleted: "+str(x+1)+"/25", end = '')

def WPspecificfiltering(videospeecharray, videotextarray, ChoiceSubreddit):
	if ChoiceSubreddit.title == "WritingPrompts":
		for x in range(0, len(videotextarray)):
			if videotextarray[x][0]=="[":
				videotextarray[x] = videotextarray[x][4:]
				videospeecharray[x] = videospeecharray[x][4:]
				#print(videotextarray[x])


def main():

	possible_subreddits = []

	WritingPrompts = subreddit_details("WritingPrompts", "The hottest writing prompts from r/WritingPrompts", "writing, prompts, writingprompts,creativity,imagination, reddit", "Art/background4.png", "Art/writing/WPoriginalother2transWhite.png", 'white', 'Art/English_Country_Garden.mp3')
	Showerthoughts = subreddit_details("Showerthoughts", "The hottest Showerthoughts from r/Showerthoughts", "Shower, Thoughts, Showerthoughts, random, intriguing, reddit", "Art/background3.png","Art/Shower/showerthoughts1transreversesand.png",'#A57164', 'Art/Sunshine_Samba.mp3')

	possible_subreddits.append(WritingPrompts)
	possible_subreddits.append(Showerthoughts)

	videotextarray = []
	videospeecharray = []
	globalchildrenlist = []

	ChoiceSubreddit = ""

	connection = sqlite3.connect('DBstuff/database.db')


	start = time.time()

	DBres.create_tables_for_subreddits(connection, possible_subreddits)

	ChoiceSubreddit = Redditres.choseSubreddit(possible_subreddits)

	globalchildrenlist = Redditres.redditAPIrequests(ChoiceSubreddit, connection, videospeecharray, videotextarray)

	WPspecificfiltering(videospeecharray, videotextarray, ChoiceSubreddit)

	pause_human_editing(globalchildrenlist)

	googleTTS.newTTScreation(videospeecharray)

	Videores.movieCreation(videotextarray, ChoiceSubreddit)

	#clean_mp3_files()

	#YouTubeAPICalls.YouTubeUpload(ChoiceSubreddit)

	end = time.time()
	format_time(start,end)

main()
