from gtts import gTTS


#a note, to not do another for loop through this content this method/logic should really go inside of
#the for loop inside reddit API requests for efficiency, however with such a small data set or 25
#this doesn't really matter at all
def speechtoTextCreation():
	for x in range(0,len(videospeecharray)):
		tts = gTTS(text=videospeecharray[x], lang='en')
		tts.save(("MP3s/"+str(x)+".mp3"))
		print("\r Created: "+str(x+1)+"/25", end = '')
	print("\n\n")
