
subscription_key = "2b0481fe8d5f443db97ae505b613c771"

class TextToSpeech(object):
	def __init__(self, subscription_key, convert_text):
		self.subscription_key = subscription_key
		self.tts = convert_text
		self.timestr = time.strftime("%Y%m%d-%H%M")
		self.access_token = None


	def save_audio(self, x):
		base_url = 'https://westus.tts.speech.microsoft.com/'
		#base_url = 'https://video-spike.search.windows.net'
		path = 'cognitiveservices/v1'
		constructed_url = base_url + path
		headers = {
			'Authorization': 'Bearer ' + self.access_token,
			'Content-Type': 'application/ssml+xml',
			'X-Microsoft-OutputFormat': 'audio-16khz-64kbitrate-mono-mp3',
			'User-Agent': 'VideoSpike'
		}
		xml_body = ElementTree.Element('speak', version='1.0')
		xml_body.set('{http://www.w3.org/XML/1998/namespace}lang', 'en-us')
		voice = ElementTree.SubElement(xml_body, 'voice')
		voice.set('{http://www.w3.org/XML/1998/namespace}lang', 'en-US')
		voice.set('name', 'Microsoft Server Speech Text to Speech Voice (en-US, Guy24KRUS)')
		voice.text = self.tts
		body = ElementTree.tostring(xml_body)

		response = requests.post(constructed_url, headers=headers, data=body)
		if response.status_code == 200:
			with open("MP3s/"+str(x) + '.mp3', 'wb') as audio:
				audio.write(response.content)
				#audio.close()
				#print("\nStatus code: " + str(response.status_code) + "\nYour TTS is ready for playback.\n")
		else:
			print("\nStatus code: " + str(response.status_code) + "\nSomething went wrong. Check your subscription key and headers.\n")



	def get_token(self):
		fetch_token_url = "https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken"
		headers = {
			'Ocp-Apim-Subscription-Key': self.subscription_key
		}
		response = requests.post(fetch_token_url, headers=headers)
		self.access_token = str(response.text)
