#!/usr/bin/env python

# Copyright 2018 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


#note this only works if you declare the environment variable
#$env:GOOGLE_APPLICATION_CREDENTIALS="C:\Users\aaron\Desktop\Apps\VideoCreator\My First Project-8236fde678b4.json"
#My First Project being the service account authentication file from the google developer console for the My First Project Project

"""Google Cloud Text-To-Speech API sample application .
Example usage:
    python quickstart.py
"""
def synthesize_text(text, outputname):
    """Synthesizes speech from the input string of text."""
    from google.cloud import texttospeech
    client = texttospeech.TextToSpeechClient()

    input_text = texttospeech.types.SynthesisInput(text=text)

    # Note: the voice can also be specified by name.
    # Names of voices can be retrieved with client.list_voices().
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)

    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)

    response = client.synthesize_speech(input_text, voice, audio_config)

    # The response's audio_content is binary.
    with open("MP3s/"+str(outputname)+'.mp3', 'wb') as out:
        out.write(response.audio_content)
        #print('Audio content written to file "test.mp3"')


def newTTScreation(videospeecharray):
	for x in range(0,len(videospeecharray)):
		synthesize_text(videospeecharray[x], x)
		#time.sleep(2.7)#because of rate limiting by microsoft azure
		print("\r Created: "+str(x+1)+"/25", end = '')
	print("\n\n")
