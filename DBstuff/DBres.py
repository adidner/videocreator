
import sqlite3

def create_tables_for_subreddits(connection, possible_subreddits):
	#check to see if all subreddits have tables and create them if they don't exists
	cursor = connection.cursor()
	for x in range(0,len(possible_subreddits)):
			sqlcommand = 'CREATE TABLE if not exists ' +possible_subreddits[x].title+ ' (id INTEGER PRIMARY KEY, title TEXT, selftext TEXT)'
			cursor.execute(sqlcommand)
	connection.commit()
	cursor.close()
