
from mutagen.mp3 import MP3

import numpy as np
from moviepy.editor import *
from moviepy.video.tools.segmenting import findObjects





def movieCreation(videotextarray, ChoiceSubreddit):
	fontsizetext = 30
	titlesize = 60
	background = ImageClip(ChoiceSubreddit.backgroundfile)
	w,h = moviesize = background.size
	screensize = background.size
	logo = ImageClip(ChoiceSubreddit.logofile).resize((h/3,h/3)).set_pos(("center","top")).set_start(0)
	textcolor = ChoiceSubreddit.color
	max_time = 180

	title = (TextClip(ChoiceSubreddit.title,fontsize=titlesize,color=textcolor)
		.set_position('center')
		 .set_duration(3)
		 .set_start(0))

	current_time = 7
	text_list = [ background, logo, title]
	aud_list = []


	for x in range(0, len(videotextarray)):

		audio = MP3("MP3s/"+str(x)+".mp3")
		clip_len = int(audio.info.length)

		#print(clip_len)

		txt_clip = ( TextClip(videotextarray[x],fontsize=fontsizetext,color=textcolor)
            .set_position('center')
             .set_duration(clip_len)
			 .set_start(current_time))

		text_list.append(txt_clip)

		#aud_clip = (AudioFileClip("MP3s/"+str(x)+".mp3").set_start(current_time).set_duration(clip_len).volumex(1.2))
		aud_clip = (AudioFileClip("MP3s/"+str(x)+".mp3").set_start(current_time).volumex(1.2))

		aud_list.append(aud_clip)
		current_time += (clip_len + 2)
		if current_time > max_time:
			break

	total_time = current_time
	audio = MP3(ChoiceSubreddit.musicfile)
	song_len = int(audio.info.length)
	song_leninout = song_len

	copy_current_time = current_time
	iteration = 0

	#aud_clip = (AudioFileClip(ChoiceSubreddit.musicfile).set_start(iteration * (song_leninout)).volumex(0.4))
	#aud_clip = aud_clip.audio_fadein(1).audio_fadeout(1)
	#aud_list.append(aud_clip)

	while True:
		if song_len > copy_current_time:
			aud_clip = (AudioFileClip(ChoiceSubreddit.musicfile).subclip(0,copy_current_time-song_len).set_start(iteration * (song_len)).volumex(0.3))
			aud_clip = aud_clip.audio_fadein(1).audio_fadeout(3)
			aud_list.append(aud_clip)
			break
		else:
			aud_clip = (AudioFileClip(ChoiceSubreddit.musicfile).set_start(iteration * (song_len)).volumex(0.3))
			aud_clip = aud_clip.audio_fadein(1).audio_fadeout(1)
			aud_list.append(aud_clip)
			iteration += 1
			copy_current_time -= song_len



	final_video = CompositeVideoClip(text_list)
	aud_total = CompositeAudioClip(aud_list)

	final_video = final_video.set_audio(aud_total)
	final_video.set_duration(current_time).write_videofile("output.mp4",fps=25)
	print("finished")
