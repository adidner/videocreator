from moviepy.editor import *

print("Starting")


background = ImageClip('background.jpg')


# Make the text. Many more options are available.
txt_clip = ( TextClip("Some form of text or lyrics of whatever",fontsize=50,color='white')
            .set_position('center')
             .set_duration(10).set_start(12) )

txt2_clip = ( TextClip("Different text also on the thing, ",fontsize=25,color='black')
            .set_position('center')
             .set_duration(10).set_start(40) )
			 
			 
result = CompositeVideoClip([background, txt_clip, txt2_clip]) # Overlay text on video




aud1 = (AudioFileClip("clip1.mp3").set_start(20).set_duration(20).volumex(1.2))
aud2 = (AudioFileClip("clip2.mp3").set_start(37).set_duration(20).volumex(1.2))


compo = CompositeAudioClip([aud1, aud2])


result = result.set_audio(compo)

result.set_duration(90).write_videofile("output.mp4",fps=25) # Many options...


print("Finished")