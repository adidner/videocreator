from moviepy.editor import *
from mutagen.mp3 import MP3


def movieCreation():
    fontsizetext = 25
    background = ImageClip('../Art/background1.png')
    w,h = moviesize = background.size
    #logo = ImageClip("../Art/Shower/showerthoughts1trans.png").resize((w/3,h/3)).set_pos(("center","top"))
    logo = ImageClip("../Art/Writing/writinglogo1trans.png").resize((w/3,h/3)).set_pos(("center","top"))

    final = CompositeVideoClip([background, logo])
    final.set_duration(60).write_videofile("testing.mp4",fps=25)

movieCreation()
