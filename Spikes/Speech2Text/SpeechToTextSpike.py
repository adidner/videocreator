


#google cloud API
def synthesize_text(text):
    """Synthesizes speech from the input string of text."""
    from google.cloud import texttospeech
    client = texttospeech.TextToSpeechClient()

    input_text = texttospeech.types.SynthesisInput(text=text)

    # Note: the voice can also be specified by name.
    # Names of voices can be retrieved with client.list_voices().
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE)

    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)

    response = client.synthesize_speech(input_text, voice, audio_config)

    # The response's audio_content is binary.
    with open('output.mp3', 'wb') as out:
        out.write(response.audio_content)
        print('Audio content written to file "output.mp3"')


#synthesize_text("Making my way downtown, walking fast, Faces pass and I\'m home bound, Staring blankly ahead just making my way, Making a way through the crowd")


from gtts import gTTS

lyrics = 'Making my way downtown, walking fast, Faces pass and I\'m home bound, Staring blankly ahead just making my way, Making a way through the crowd'

tts = gTTS(text=lyrics, lang='en')
tts.save("lyrics.mp3")



#With Watson
API_KEY = "FYVYSAu9dl9hwCyi4JB4FurL1GvvCI7HDUj76cFa0rHo"
URL = "https://stream.watsonplatform.net/text-to-speech/api"

from watson_developer_cloud import TextToSpeechV1
import json

text_to_speech = TextToSpeechV1(
    iam_apikey=API_KEY,
    url=URL
)

with open('hello_world.wav', 'wb') as audio_file:
    audio_file.write(
        text_to_speech.synthesize(
            lyrics,
            'audio/wav',
            'en-US_AllisonVoice'
        ).get_result().content)
		
		
from mutagen.mp3 import MP3
audio = MP3("lyrics.mp3")
print(audio.info.length)