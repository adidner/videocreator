import os, requests, time
from xml.etree import ElementTree



#https://portal.azure.com/#@didner.com/resource/subscriptions/7d572c04-818f-4f3c-bcc3-e97ff2786e57/resourceGroups/VideoSpike/providers/Microsoft.CognitiveServices/accounts/VideoSpike/keys
#https://azure.microsoft.com/en-us/pricing/details/cognitive-services/speech-services/
#https://docs.microsoft.com/en-us/azure/search/search-security-api-keys
#https://github.com/Azure-Samples/Cognitive-Speech-TTS/blob/master/Samples-Http/Python/TTSSample.py
#https://docs.microsoft.com/en-us/azure/cognitive-services/speech-service/quickstart-python-text-to-speech



class TextToSpeech(object):
	def __init__(self, subscription_key):
		self.subscription_key = subscription_key
		self.tts = input("What would you like to convert to speech: ")
		self.timestr = time.strftime("%Y%m%d-%H%M")
		self.access_token = None
		
	
	def save_audio(self):
		base_url = 'https://westus.tts.speech.microsoft.com/'
		#base_url = 'https://video-spike.search.windows.net'
		path = 'cognitiveservices/v1'
		constructed_url = base_url + path
		headers = {
			'Authorization': 'Bearer ' + self.access_token,
			'Content-Type': 'application/ssml+xml',
			'X-Microsoft-OutputFormat': 'riff-24khz-16bit-mono-pcm',
			'User-Agent': 'VideoSpike'
		}
		xml_body = ElementTree.Element('speak', version='1.0')
		xml_body.set('{http://www.w3.org/XML/1998/namespace}lang', 'en-us')
		voice = ElementTree.SubElement(xml_body, 'voice')
		voice.set('{http://www.w3.org/XML/1998/namespace}lang', 'en-US')
		voice.set('name', 'Microsoft Server Speech Text to Speech Voice (en-US, Guy24KRUS)')
		voice.text = self.tts
		body = ElementTree.tostring(xml_body)

		response = requests.post(constructed_url, headers=headers, data=body)
		if response.status_code == 200:
			with open('sample-' + self.timestr + '.mp3', 'wb') as audio:
				audio.write(response.content)
				print("\nStatus code: " + str(response.status_code) + "\nYour TTS is ready for playback.\n")
		else:
			print("\nStatus code: " + str(response.status_code) + "\nSomething went wrong. Check your subscription key and headers.\n")
	
	
	
	def get_token(self):
		fetch_token_url = "https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken"
		headers = {
			'Ocp-Apim-Subscription-Key': self.subscription_key
		}
		response = requests.post(fetch_token_url, headers=headers)
		self.access_token = str(response.text)


def main():
    subscription_key = "2b0481fe8d5f443db97ae505b613c771"
    app = TextToSpeech(subscription_key)
    app.get_token()
    app.save_audio()
	
main()