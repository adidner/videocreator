


def choseSubreddit(possible_subreddits):
	print("Choose which subreddit to build around, options: ")
	for x in range(0, len(possible_subreddits)):
		print(str(x) + ": " + possible_subreddits[x].title)
	answer = input("Choice? ")
	return possible_subreddits[int(answer)]



REDDIT_BASE_URL = "https://www.reddit.com/r/"
DEFAULT_LISTING_TYPE = "/hot"
RETURN_TYPE = ".json"
LIMITER = "?limit=1"



num_chars_per_line = 70


import requests
import json

length_limit = 1000

def redditAPIrequests(ChoiceSubreddit, connection, videospeecharray, videotextarray):

	url = build_url(ChoiceSubreddit)
	response = requests.get(url, headers = {'User-agent': 'ChannelChannelPage 0.1'})
	theJSON = json.loads(response.text)
	childrenlist = theJSON.get("data").get("children")


	for x in range(0,len(childrenlist)):
		#print(childrenlist[x].get("data").get("title"))
		#print(childrenlist[x].get("data").get("selftext"))
		returntext = filters(childrenlist[x], ChoiceSubreddit, connection)
		if returntext=="":
			print("bad child")
		else:
			#print(childrenlist[x].get("data"))
			videospeecharray.append(returntext)
			returntext = insert_newlines(returntext)
			videotextarray.append(returntext)

			print(str(x)+": " + returntext)
	print("")
	return childrenlist

def build_url(subreddit):
	return REDDIT_BASE_URL + subreddit.title + DEFAULT_LISTING_TYPE + RETURN_TYPE;




def filters(reddit_post_element, ChoiceSubreddit, connection):
	#print("in filters")
	if filter_picture(reddit_post_element):
		#print("pic filter")
		return ""
	#if filter_link(reddit_post_element):
	#	print("link filter")
	#	return ""
	if filter_length(reddit_post_element):
		#print("length filter")
		return ""
	if filter_video(reddit_post_element):
		#print("vid filter")
		return ""
	#if filter_repeat_text(connection, reddit_post_element, ChoiceSubreddit):
	#	return ""
	finaltext =  filter_selftext_or_title(reddit_post_element)


	return finaltext



def filter_picture(reddit_post_element):
	#ideas
	#is_reddit_media_domain = true
	#
	#media = {} vs media = null for text posts
	#is_video = true is bad
	#url ending in jpg or png
	#print("in pic filters")
	#print(reddit_post_element.get("data"))
	if reddit_post_element.get("data").get("is_reddit_media_domain"):
		#print("pic 1")
		return True
	if "jpg" in reddit_post_element.get("data").get("url"):
		#print("pic 2")
		return True
	if "png" in reddit_post_element.get("data").get("url"):
		#print("pic 3")
		return True
	if "gif" in reddit_post_element.get("data").get("url"):
		#print("pic 3.gif")
		return True
	if reddit_post_element.get("data").get("media") is not None:
		#print("pic 4")
		return True
	else:
		return False

def filter_video(reddit_post_element):
	if reddit_post_element.get("data").get("is_video"):
		return True
	else:
		return False

def filter_link(reddit_post_element):
	if reddit_post_element:
		return True
	else:
		return False


def filter_length(reddit_post_element):
	if len(reddit_post_element.get("data").get("selftext")) > length_limit:
		return True
	else:
		return False


def filter_selftext_or_title(reddit_post_element):
	title = reddit_post_element.get("data").get("title")
	selftext = reddit_post_element.get("data").get("selftext")
	if len(title) > len(selftext):
		return title
	else:
		return selftext

def filter_repeat_text(connection, reddit_post_element, ChoiceSubreddit):
	#print("from filter ChoiceSubreddit: "+ChoiceSubreddit)
	title = reddit_post_element.get("data").get("title")
	selftext = reddit_post_element.get("data").get("selftext")

	cursor = connection.cursor()
	sqlitecommand = 'SELECT * FROM '+ChoiceSubreddit.title+' WHERE title=? AND selftext=?'
	#print(sqlitecommand)
	cursor.execute(sqlitecommand, (title,selftext))
	row = cursor.fetchone()
	#2 cases,
	#if see that its a copy, return true
	if row is None:
		print('Inserting')
		cursor.execute('INSERT INTO '+ChoiceSubreddit.title+'(title, selftext) VALUES(?, ?)', (title,selftext))
		connection.commit()
		return False
	#else, not a copy
	#add to # DB, return false
	else:
		#we have a copy
		print('copy, ingnored,')
		return True
	cursor.close()


def insert_newlines(target_string):
	#find a space actually
	count = 0
	insert = False
	newstringentry = ""
	for char in target_string:
		newstringentry = newstringentry + char
		if count > num_chars_per_line:
			insert = True
		if insert and char==" ":
			newstringentry = newstringentry + "\n"
			insert = False
			count = 0
		count+=1
	return newstringentry
